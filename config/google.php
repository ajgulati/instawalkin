<?php

return [
    'license_type' => env('GOOGLE_LICENSE_TYPE', 'standard'),

    'key' => env('GOOGLE_KEY', 'AIzaSyA6o6f_keYJ6nsnswosjOL1pNAQpX06Pak'),

    'client_id'      => env('GOOGLE_CLIENT_ID', ''),
    'encryption_key' => env('GOOGLE_ENCRYPTION_KEY', ''),
];